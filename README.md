# Stack và lỗi Buffer Overflow

## Stack là gì?

Stack là vùng bộ nhớ mà chương trình dùng để lưu trữ các biến cục bộ của một hàm khi hàm đó đang chạy. Stack phát triển từ trên xuống, và thứ tự của các biến trên stack sẽ giống với thứ tự được khai báo của biến đó trong mã nguồn của chương trình.

Lấy ví dụ đoạn chương trình như sau

```c++
int main()
{
    int a, b;
    char name[16];
}
```
thì trạng thái của stack khi chạy đến hàm `main` là:

![](./Image/STACK_1.png)

Mỗi biến đều có một địa chỉ của nó. Địa chỉ của một biến được tính là ô nhớ đầu tiên của vùng nhớ do biến đó chiếm giữ. Do stack phát triển từ trên xuống, nên các biến được cấp phát trước sẽ có địa chỉ cao hơn địa chỉ của các biến được cấp phát sau. Biến `a` nằm ở địa chỉ 1000 và chiếm giữ 4 ô nhớ từ 1000 đến hết ô 1003 (do kiểu *int* có kích thước 4 bytes). Biến `b` có địa chỉ 996 và chiếm giữ 4 ô nhớ từ 996 đến hết ô 999. Tương tự, biến `name` có địa chỉ 980, và chiếm giữ các ô nhớ từ 980 đến hết ô 995.

## Lỗi Buffer Overflow

Lỗi buffer overflow (còn gọi là lỗi tràn bộ đệm) là lỗi xảy ra khi dữ liệu nhập vào cho một biến vượt quá giới hạn lưu trữ của nó.

Lại lấy đoạn chương trình ở trên làm ví dụ, biến `name` được khai báo là `char[16]` và chỉ có thể chứa được tối đa 16 kí tự. Vậy nếu chúng ta nhập vào nhiều hơn 16 kí tự thì sao?

Câu trả lời là phần dữ liệu bị dư đó sẽ ghi đè lên các biến nằm sau biến `name` trên stack, cụ thể là biến `b` sẽ bị ghi đè trước, sau đó là biến `a`. Nếu vẫn tiếp tục nhập thì dữ liệu sẽ ghi đè lên những phần tiếp theo trên stack và làm cho chương trình bị crash.

Để dễ hình dung hơn thì bạn có thể tưởng tượng một stack nẳm ngang thay vì nằm dọc như ở trên, và dữ liệu sẽ được ghi vào các biến từ trái qua phải như cách chúng ta vẫn thường viết hàng ngày.

![](./Image/OVERFLOW.png)

## Luyện tập

Trong thư mục **example** có một chương trình bị lỗi buffer overflow, bạn có thể tận dụng lỗi này để sửa giá trị của hai biến `a` và `b` thành giá trị khác 0 được hay không? Có thể sửa thành một giá trị tùy ý hay không?

## Thông tin thêm

Dù đã xuất hiện từ rất lâu nhưng cho đến thời điểm hiện tại, buffer overflow vẫn là một trong những lỗi phổ biến và nguy hiểm bậc nhất. Các bạn có thể tham khảo danh sách các trường hợp lỗi đã được phát hiện và công bố [tại đây](https://cve.mitre.org/cgi-bin/cvekey.cgi?keyword=buffer+overflow).

(Trick) Lỗi buffer overflow có thể được phát hiện bằng cách nhập thật nhiều dữ liệu vào chương trình, nếu chương trình bị crash hoặc có những biểu hiện "lạ" thì nhiều khả năng là chương trình đó có lỗi buffer overflow.
